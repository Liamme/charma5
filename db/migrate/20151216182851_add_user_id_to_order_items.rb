class AddUserIdToOrderItems < ActiveRecord::Migration
  def change
    add_column :order_items, :user_id, :integer
    add_reference :order_items, :order_items, index: true, foreign_key: true
  end
end
