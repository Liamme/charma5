class AddSurnameToAdmins < ActiveRecord::Migration
  def change
    add_column :admins, :surname, :string
  end
end
