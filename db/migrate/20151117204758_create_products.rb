class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :title
      t.string :short_des
      t.text :long_des
      t.decimal :price
      t.string :image_name
      t.integer :discount
      t.string :category
      t.string :sub_category
      t.string :image_2
      t.string :inage_3

      t.timestamps null: false
    end
  end
end
