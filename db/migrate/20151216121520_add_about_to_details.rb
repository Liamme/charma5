class AddAboutToDetails < ActiveRecord::Migration
  def change
    add_column :details, :about, :string
  end
end
