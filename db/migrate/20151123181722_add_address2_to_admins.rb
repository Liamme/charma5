class AddAddress2ToAdmins < ActiveRecord::Migration
  def change
    add_column :admins, :address2, :string
  end
end
