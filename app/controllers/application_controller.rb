class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  #protect_from_forgery with: :exception
  helper_method :get_total, :create_cart
 before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :create_cart
  
helper_method :current_order
 
 @details = Detail.all
 
 def current_order
    if !session[:order_id].nil?
      Order.find(session[:order_id])
    else
      Order.new
  end
 end
 
 
 
 def users
  @products = Product.all
  @details = Detail.all
 end

 
 protected
 
 def configure_permitted_parameters
   devise_parameter_sanitizer.for(:sign_up) { |u| u.permit(:name, :surname, :address1, :address2, :address3, :address4, :email, :password, :password_confirmation, :remember_me) }
   devise_parameter_sanitizer.for(:sign_in) { |u| u.permit(:login, :name, :surname,  :address1, :address2, :address3, :address4, :email, :password, :remember_me) }
   devise_parameter_sanitizer.for(:sign_up) { |u| u.permit(:name, :surname,  :address1, :address2, :address3, :address4, :email, :password, :password_confirmation, :current_password) }
   devise_parameter_sanitizer.for(:account_update) { |u| u.permit(:name, :surname,  :address1, :address2, :address3, :address4, :email, :password, :password_confirmation, :current_password) }
 end
 
 def create_cart
    if session[:cart] then
      @cart = session[:cart] 
    else
      @cart = {}
    end
 end


#def get_total
    
  #  total = 0
 #   @cart.each do |id, quantity|
#      product = Product.find_by_id(id)
 #     sub_total = quantity * product.price
#      total+= sub_total
#   end
    
  #  return total

# end

  


end


