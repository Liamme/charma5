class DetailsController < ApplicationController
  before_action :set_detail, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_admin!
  # GET /details
  # GET /details.json
  def index
    @details = Detail.all
   @products = Product.all
   @orders = Order.all
   @users= User.all
    @order_items = current_order.order_items
@orderItem = OrderItem.all
  end

  # GET /details/1
  # GET /details/1.json
  def show
    @details = Detail.all
  end

  # GET /details/new
  def new
    @detail = Detail.new
    @details = Detail.all
  end

  # GET /details/1/edit
  def edit
    @details = Detail.all
  end

  # POST /details
  # POST /details.json
  def create
    
    
    @detail = Detail.new(detail_params)
@details = Detail.all
    respond_to do |format|
      if @detail.save
        format.html { redirect_to @detail, notice: 'Detail was successfully created.' }
        format.json { render :show, status: :created, location: @detail }
      else
        format.html { render :new }
        format.json { render json: @detail.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /details/1
  # PATCH/PUT /details/1.json
  def update
    respond_to do |format|
      if @detail.update(detail_params)
        format.html { redirect_to @detail, notice: 'Detail were successfully updated.' }
        format.json { render :show, status: :ok, location: @detail }
      else
        format.html { render :edit }
        format.json { render json: @detail.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /details/1
  # DELETE /details/1.json
  def destroy
    @detail.destroy
    respond_to do |format|
      format.html { redirect_to details_url, notice: 'Detail was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_detail
      @detail = Detail.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def detail_params
      params.require(:detail).permit(:welcome, :about)
    end
end
