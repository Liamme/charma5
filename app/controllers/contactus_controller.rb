class ContactusController < ApplicationController


    def new
      @contactus = ContactForm.new
    end

    def create
      begin
        @contactus = ContactForm.new(params[:contactus])
        @contactus.request = request
        if @contactus.deliver
          flash.now[:notice] = 'Thank you for your message!'
        else
          render :new
        end
      rescue ScriptError
        flash[:error] = 'Sorry, this message appears to be spam and was not delivered.'
      end
    end

  
end
