json.array!(@products) do |product|
  json.extract! product, :id, :title, :short_des, :long_des, :price, :image_name, :discount, :category, :sub_category, :image_2, :inage_3
  json.url product_url(product, format: :json)
end
