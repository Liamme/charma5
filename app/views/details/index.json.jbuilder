json.array!(@details) do |detail|
  json.extract! detail, :id, :welcome, :about
  json.url detail_url(detail, format: :json)
end
