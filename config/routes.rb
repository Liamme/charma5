Rails.application.routes.draw do
  
  resources :details
  
  post "/" => 'paypal#handle'
  post '/paypal' => 'paypal#validate'
  
  get 'static_page/admin'

  get 'order_items/create'

  get 'order_items/update'

  get 'order_items/destroy'

  get 'order_items/create'

  get 'order_items/update'

  get 'order_items/destroy'


 get 'admins/sign_up' => 'page#home' #this stops unauthorised admin sign up
  devise_for :admins
  
 


  
  get '/cart' => 'cart#index'
  get '/cart/order/:id' => 'cart#order'
  
  get '/cart/clear' => 'cart#clearCart'
 
  get '/cart/remove/:id' => 'cart#remove'
  get '/cart/decrease/:id' => 'cart#decrease'

  resources :products
  resources :products
  
  
  resources :products, only: [:index]
  resource :cart, only: [:show]
  resources :order_items, only: [:create, :update, :destroy]
  
  
 post '/search' => 'products#search'

resources :contactus
get 'page/contactus'
  
  get 'contactus/index'

  devise_for :users
  
  root 'page#home'
  
  resources :users
  
  resources :orders
  get 'orders/confirm/:id' => 'orders#confirm'
  
  
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
