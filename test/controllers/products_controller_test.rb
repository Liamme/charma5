require 'test_helper'

class ProductsControllerTest < ActionController::TestCase
  setup do
    @product = products(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:products)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create product" do
    assert_difference('Product.count') do
      post :create, product: { category: @product.category, discount: @product.discount, image_2: @product.image_2, image_name: @product.image_name, inage_3: @product.inage_3, long_des: @product.long_des, price: @product.price, short_des: @product.short_des, sub_category: @product.sub_category, title: @product.title }
    end

    assert_redirected_to product_path(assigns(:product))
  end

  test "should show product" do
    get :show, id: @product
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @product
    assert_response :success
  end

  test "should update product" do
    patch :update, id: @product, product: { category: @product.category, discount: @product.discount, image_2: @product.image_2, image_name: @product.image_name, inage_3: @product.inage_3, long_des: @product.long_des, price: @product.price, short_des: @product.short_des, sub_category: @product.sub_category, title: @product.title }
    assert_redirected_to product_path(assigns(:product))
  end

  test "should destroy product" do
    assert_difference('Product.count', -1) do
      delete :destroy, id: @product
    end

    assert_redirected_to products_path
  end
end
